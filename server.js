//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/cmelani/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var movimientosJSONV2 = require('./movimientosV2.json');

app.listen(port);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'))
});


app.get('/v1/movimientos', (req, res) => {
  res.sendFile(path.join(__dirname, `movimientosV1.json`))
})

app.get('/v2/movimientos', (req, res) => {
  res.send(movimientosJSONV2)
})

app.get('/v2/movimientos/:id', (req, res) => {
  res.send(movimientosJSONV2[req.params.id])
})

app.get('/v2/movimientosq', (req, res) => {
  console.log(req.query)
  res.send(req.query)
})

app.get('/clientes/:idCliente', (req, res) => {
  res.send(`Aqui tiene el cliente numero ${req.params.idCliente}`)
})


app.post('/v2/movimientos', ({body}, res) => {
  body.id = movimientosJSONV2.length ++;
  movimientosJSONV2.push(body);
  res.status(200).send("Se agregó el elemento:" + JSON.stringify(body) + "\n EN \n"  +  JSON.stringify(movimientosJSONV2));
})

app.post('/', ({body}, res) => {
  res.send("Hemos recibido su peticion post cambiada");
})


app.get('/movimientos', function(req, res) {
  clienteMLab.get('', function(err, resM, body) {
   if(err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/movimientos', function(req, res) {
  clienteMLab.post('', req.body, function(err, resM, body) {
      res.send(body);
  })
})


console.log('todo list RESTful API server started on: ' + port);
